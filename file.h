#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED


void loadFile(Arbre *arbre);
void write_child(Personne *a , Personne *b);
void write_couple(Personne *a , Personne *b);
void write_root(Personne *a);
void write_modify_sexe(Personne *a);
void write_modify_nom(Personne *a , char nouveau_nom[50]);
void write_modify_prenom(Personne *a , char nouveau_prenom[50]);
void write_delete(char nom[50]);
void reinitialize();
#endif // FILE_H_INCLUDED
