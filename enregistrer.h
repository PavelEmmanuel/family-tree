#ifndef ENREGISTRER_H_INCLUDED
#define ENREGISTRER_H_INCLUDED

#include "Personne.h"

void saisirPersonne(Personne* personne);
void modifierPersonne(Personne *personne);
void ajouterPersonne(Arbre *arbre);
void ajouterFrere(Personne* a, Personne* b);
void ajouterEnfant(Personne* a,Personne* b);
void initialiserPersonne(Personne *personne);
void ajouterConjoint(Personne *a , Personne *b);
void deletePersonne(Arbre* arbre , Personne* personne);
void destroy(Arbre *arbre, Personne* ptr);
void destroyNode(Personne *ptr);

#endif // ENREGISTRER_H_INCLUDED
