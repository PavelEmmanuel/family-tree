#ifndef RELATION_H_INCLUDED
#define RELATION_H_INCLUDED

#include "Personne.h"

Personne* traverseDown(Personne* ptr, char s[50]);
Personne* traverseRight(Personne* ptr, char s[50]);
Personne* search(Arbre* arbre, char s[50]);
void findRelationBetween(Arbre *arbre);
void findRelation(Personne *personne , int relation , Personne *racine);
void searchMultiple(Personne *personne , char s[50] , Personne **ptr );
int getHeightOfNode(Personne *racine , Personne *personne , int level);
char *strlower(char *str);

#endif // RELATION_H_INCLUDED
