#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "Personne.h"
#include "enregistrer.h"
#include "relation.h"
#include "file.h"

#define CLEAR "cls"

void show(Personne* personne);
void display(Personne *racine , Personne* personne);

void afficheDebut(){
    printf("\t\t\t+----------------------------------+\n");
    printf("\t\t\t|     Family Tree Program v1.0     |\n");
    printf("\t\t\t+----------------------------------+\n\n\n");
}

int main()
{
    int opt , c;
    char d;
    Arbre* arbre = (Arbre *) malloc(sizeof(Arbre));
    arbre->racine = NULL;

    afficheDebut();
    printf("Chargement du fichier ...\n");
    loadFile(arbre);

    while(1){
        system(CLEAR);
        afficheDebut();
        printf("\t1. Ajouter une personne\n\t2. Trouver relation entre 2 personnes\n\t3. Rechercher\n\t4. Supprimer une personne \n\t5. Afficher l'arbre\n\t6. Modifier une personne\n\t7. Trouver relation\n\t8. Reinitialiser les donnees");
        printf("\n\n\t Entrez votre choix : ");
        scanf("%d" , &opt);
        while ((c = getchar()) != '\n' && c != EOF) { }
        switch(opt){
            case 1 :
                system(CLEAR);
                afficheDebut();
                printf("==> Ajouter une personne\n\n");
                ajouterPersonne(arbre);
                break;
            case 2:
                system(CLEAR);
                afficheDebut();
                findRelationBetween(arbre);
                break;
            case 3:{
                system(CLEAR);
                int opt2;
                afficheDebut();
                char nom[50];
                Personne *personne = NULL;
                printf("\tEntrer le nom de la personne a rechercher : ");
                scanf("%s" , nom);
                while ((c = getchar()) != '\n' && c != EOF) { }
                printf("\n");
                personne = search(arbre , strlower(nom));
                if(personne){
                    show(personne);
                    printf("\nVoulez vous effectuer des operations sur %s ?\n\n\t1. Ajouter un menbre de la famille\n\t2. Trouver relation\n\t3. Modifier les informations\n\t4. Quitter\n", personne->nom);
                    printf("\tEntrez votre choix : ");
                    scanf("%d" , &opt2);
                    switch(opt2){
                        case 1 :{
                            printf("Ajouter un menbre de la famille a : %s %s\n" , personne->nom , personne->prenom);
                            char child[10];
                            char brother[10];
                            char adjectif[3];
                            int opt3 ;
                            Personne *nouvellePersonne = (Personne * ) malloc(sizeof(Personne));
                            initialiserPersonne(nouvellePersonne);
                            saisirPersonne(nouvellePersonne);
                            strcpy(child , "fille");
                            strcpy(brother , "soeur");
                            strcpy(adjectif , "la");
                            if(personne->sexe == 'M'){
                                strcpy(child , "fils");
                                strcpy(brother , "frere");
                                strcpy(adjectif , "le");
                            }
                            printf("Entrez le nom de la relation \n 1 - %s \n 2 - %s\n 3 - Conjoint(e)\n" , child , brother);
                            printf("%s est %s ______ de %s : " , nouvellePersonne->nom , adjectif , personne->nom);
                            scanf("%d", &opt3);
                            while ((c = getchar()) != '\n' && c != EOF) { }
                            switch(opt3){
                                case 1 :
                                    ajouterEnfant(personne, nouvellePersonne);
                                    printf("Ajout effectuer avec success\n");
                                    break;
                                case 2:
                                    ajouterFrere(personne, nouvellePersonne);
                                    printf("Ajout effectuer avec success\n");
                                    break;
                                case 3 :
                                    ajouterConjoint(personne , nouvellePersonne);
                                    printf("Ajout effectuer avec success\n");
                                    break;
                                default :
                                    printf("La valeur saisi ne figure pas dans la liste ! Reessayez\n");
                            }
                            break;
                        }
                        case 2 : {
                            int opt4;
                            printf("Entrez le nom de la relation \n 1 - conjoint \n 2 - parents \n 3 - Freres \n 4 - Cousins \n 5 - Oncles et Tantes \n 6 - Oncles \n 7 - Tantes \n 8 - Grands Parents \n 9 - Heritiers(Oncles maternels) \n 10 - Oncles paternels \n 11 - Belle soeur \n 12 - Beau frere \n 13 - Enfants \n 14 - Filles \n 15 - Fils \n");
                            printf("Trouver le(s) ______ de %s : " , personne->nom);
                            scanf("%d" , &opt4);
                            while ((c = getchar()) != '\n' && c != EOF) { };
                            findRelation(personne , opt4 , arbre->racine);
                            break;
                        }
                        case 3 : {
                            modifierPersonne(personne);
                            break;
                        }
                        case 4 : {
                            break;
                        }
                        default :{
                            printf("Oups ! la valeur saisie est incorrecte");
                            break;
                        }
                    }
                }
                else{
                   printf("Le nom entre n'existe pas dans l'arbre genealogique\n");
                }
                break;
            }
            case 4 : {
                char name[50];
                Personne *personne = NULL;
                printf("Entrer le nom de la personne a retirer de l'arbre : ");
                scanf("%s"  ,name);
                while ((c = getchar()) != '\n' && c != EOF) { }
                personne = search(arbre , strlower(name));
                if(personne){
                    show(personne);
                    char choice;
                    printf("Etes vous sur de vouloir supprimer cette personne ?\nCette operation entrainera la suppression de la personne ainsi que ca descendance (y/n): ");
                    scanf("%c" , &choice);
                    while ((c = getchar()) != '\n' && c != EOF) { }
                    c = toupper(choice);
                    if(c == 'Y'){
                        deletePersonne(arbre , personne);
                    }else{
                        printf("Annulation de la suppression\n");
                    }
                }else{
                    printf("Le nom entre n'existe pas dans l'arbre genealogique\n");
                }
                break;
            }
            case 5:{
                system(CLEAR);
                afficheDebut();
                if(arbre->racine){
                    display(arbre->racine , arbre->racine);
                }else{
                    printf("Oups ! Aucune donnee dans l'arbre\n");
                }
                break;
            }
            case 6:{
                system(CLEAR);
                afficheDebut();
                char nom[50];
                Personne *personne = NULL;
                printf("Entrer le nom de la personne a modifier : ");
                scanf("%s" , nom);
                while ((c = getchar()) != '\n' && c != EOF) { };
                personne = search(arbre , strlower(nom));
                if(personne){
                    show(personne);
                    modifierPersonne(personne);
                }else{
                    printf("Oups la personne rechercher est introuvable!\n");
                }
                break;
            }
            case 7 : {
                int opt;
                system(CLEAR);
                afficheDebut();
                char nom[50];
                printf("Entrer le nom de la personne : ");
                scanf("%s" , nom);
                while ((c = getchar()) != '\n' && c != EOF) { };
                Personne* personne = search(arbre , strlower(nom));
                if(personne){
                    printf("Entrez le nom de la relation \n 1 - conjoint \n 2 - parents \n 3 - Freres \n 4 - Cousins \n 5 - Oncles et Tantes \n 6 - Oncles \n 7 - Tantes \n 8 - Grands Parents \n 9 - Heritiers(Oncles maternels) \n 10 - Oncles paternels \n 11 - Tantes Maternelles \n 12 - Tantes paternels \n 13 - Belle soeur \n 14 - Beau frere \n 15 - Enfants \n 16 - Filles \n 17 - Fils \n");
                    printf("Trouver le(s) ______ de %s : " , nom);
                    scanf("%d" , &opt);
                    while ((c = getchar()) != '\n' && c != EOF) { };
                    findRelation(personne , opt , arbre->racine);
                }else{
                    printf("Le nom saisi ne fait pas partie de la famille ! R�essayez !\n");
                }
                break;
            }
            case 8 : {
                //system(CLEAR);
                //afficheDebut();
                char opt;
                printf("Etes vous sur de vouloir reinitialiser le fichier (y/n) : ");
                scanf("%c" , &opt);
                while ((c = getchar()) != '\n' && c != EOF) { }
                opt = toupper(opt);
                if(opt == 'Y'){
                    printf("Reinitialisation du fichier...\n");
                    reinitialize();
                    destroy(arbre , arbre->racine);
                }else{
                    printf("Annulation de la reinitialisation\n");
                }
                break;
            }
            case 11 : {
                system(CLEAR);
                afficheDebut();
                Personne **ptr = (Personne **) malloc(sizeof(Personne*) * 1);
                searchMultiple(arbre->racine , "emma" , ptr);
                for(int i = 0 ; i< sizeof(ptr)/sizeof(Personne *) ; i++){
                    printf("%s" , ptr[i]->nom);
                }
                break;
            }
            case 12:{
                //system(CLEAR);
                //afficheDebut();
                printf("Chargement du fichier ...\n");
                loadFile(arbre);
                break;
            }
            default :{
                printf("La valeur saisie ne fait pas partie de la liste d'options\n");
            }
        }
        printf("Appuyez sur une touche pour continuer... ");
        scanf("%s", &d);
        while ((c = getchar()) != '\n' && c != EOF) { }
    }
    return 0;
}




void show(Personne* personne)
{
    char sexe[10];
    strcpy(sexe,"Feminin");
    if(personne->sexe == 'M')
       strcpy(sexe,"Masculin");
    printf("******************************************\n");
    printf("Nom : %s\nPrenoms: %s\nSexe: %s\n" , personne->nom , personne->prenom , sexe);
    printf("******************************************\n");
}




void display(Personne *racine, Personne* ptr)
{

    if(ptr==NULL)
        return;

    display(racine , ptr->frere_cadet);
    int heighOfNode = getHeightOfNode(racine , ptr , 0);
    for(int i =0 ; i< heighOfNode ; i ++){
        printf("\t");
    }

    if(ptr->conjoint != NULL){
        printf("| --> %s %s & %s %s\n" , ptr->nom  , ptr->prenom , ptr->conjoint->nom , ptr->conjoint->prenom);
    }else{
        printf("| --> %s %s\n" , ptr->nom , ptr->prenom);
    }
    display(racine , ptr->first_child);
}




