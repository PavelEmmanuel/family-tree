#ifndef PERSONNE_H_INCLUDED
#define PERSONNE_H_INCLUDED

typedef struct Personne Personne;
typedef struct Arbre Arbre;

struct Personne
{
    char nom[50];
    char prenom[50];
    char sexe;
    Personne* parent;
    Personne* first_child;
    Personne* frere_cadet;
    Personne* conjoint;
};

struct Arbre
{
    Personne *racine;
};

#endif // PERSONNE_H_INCLUDED
