#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


#include "relation.h"
#include "enregistrer.h"

#define TAILLE_MAX 150
#define CREATE_ROOT_INSTRUCTION "CREATE_ROOT"
#define ADD_CHILD_INSTRUCTION "ADD_CHILD"
#define MAKE_COUPLE_INSTRUCTION "MAKE_COUPLE"
#define DELETE_INSTRUCTION "DELETE"
#define FILENAME "FamilyTree.txt"
#define MODIFY_INSTRUCTION "MODIFY"


void loadFile(Arbre *arbre){
    FILE *file = NULL;
    char chaine[TAILLE_MAX] = "";
    char *ptr;
    char cmd[6][20];

    file = fopen(FILENAME , "r");

    if (file){
        while(fgets(chaine , TAILLE_MAX , file) != NULL){
            int i =0;
            ptr = strtok(chaine , "/");
            while(ptr != NULL){
                strcpy(cmd[i] , ptr);
                ptr = strtok(NULL , "/");
                i +=1;
            }
            if (strcmp(cmd[0] , CREATE_ROOT_INSTRUCTION) == 0){
                Personne *personne = (Personne *) malloc(sizeof(Personne));
                initialiserPersonne(personne);
                strcpy(personne->nom , cmd[1]);
                strcpy(personne->prenom , cmd[2]);
                personne->sexe = toupper(cmd[3][0]);
                arbre->racine = personne;
            }
            else if(strcmp(cmd[0] , ADD_CHILD_INSTRUCTION) == 0){
                Personne *child = (Personne *) malloc(sizeof(Personne));
                Personne *parent = NULL;

                initialiserPersonne(child);
                strcpy(child->nom , cmd[2]);
                strcpy(child->prenom , cmd[3]);
                child->sexe = toupper(cmd[4][0]);

                parent  = search(arbre , strlower(cmd[1]));

                if(parent){
                    ajouterEnfant(parent , child);
                }else{
                    printf("Oups ! Echec de l'ajout d'enfant\n");
                }
            }
            else if(strcmp(cmd[0] , MAKE_COUPLE_INSTRUCTION) == 0){
                Personne *conjoint = (Personne *) malloc(sizeof(Personne));
                Personne *personne = NULL;

                initialiserPersonne(conjoint);
                strcpy(conjoint->nom , cmd[2]);
                strcpy(conjoint->prenom , cmd[3]);
                conjoint->sexe = toupper(cmd[4][0]);
                personne = search(arbre , strlower(cmd[1]));
                if(personne){
                    ajouterConjoint(personne , conjoint);
                }else{
                    printf("Oups ! Echec de l'ajout du conjoint\n");
                }
            }
            else if(strcmp(cmd[0] , MODIFY_INSTRUCTION) == 0){
                // MODIFY/nom/emmanuel/koffi kouassi
                Personne *personne = search(arbre , cmd[2]);
                if(strcmp(cmd[1] , "nom" )== 0 ){
                    strcpy(personne->nom , cmd[3]);
                }else if (strcmp(cmd[1] , "prenom") == 0 ){
                    strcpy(personne->prenom , cmd[3]);
                }else if(strcmp(cmd[1] , "sexe") == 0){
                    personne->sexe = 'M';
                }else{
                    printf("Oups choix incorrect");
                }
            }
            else if(strcmp(cmd[0] , DELETE_INSTRUCTION) == 0){
                //DELETE/emmanuel
                Personne *personne = search(arbre , cmd[1]);
                if(personne){
                    deletePersonne(arbre , personne);
                }else{
                    printf("can't find people");
                }
            }
        }
        printf("Fichier charge avec success\n");
        fclose(file);
    }else{
        printf("Oups! Echec de l'ouverture du fichier\n");
    }
}

void reinitialize(){
    FILE *fichier = NULL;
    fichier = fopen(FILENAME , "w+");
    if(fichier){
        printf("Reinitialisation effectue avec succes\n");
    }else{
        printf("Echec de reinitialisation du fichier\n");
    }
    fclose(fichier);
}

void write_root(Personne *a){
    FILE* fichier = NULL;
    fichier = fopen(FILENAME, "w");

    if (fichier)
    {
        fprintf(fichier , "CREATE_ROOT/%s/%s/%c" , a->nom , a->prenom , a->sexe);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }

    fclose(fichier);
}


void write_child(Personne *a , Personne *b){
    FILE* fichier = NULL;

    fichier = fopen(FILENAME, "a");

    if (fichier)
    {
        fprintf(fichier , "\nADD_CHILD/%s/%s/%s/%c" , a->nom , b->nom , b->prenom , b->sexe);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }

    fclose(fichier);
}


void write_modify_nom(Personne *a , char nouveau_nom[50]){
    // MODIFY/nom/emmanuel/koffi kouassi
    FILE* fichier = NULL;
    fichier = fopen(FILENAME , "a");

    if (fichier)
    {
        fprintf(fichier , "\nMODIFY/nom/%s/%s" , a->nom , nouveau_nom);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }
    fclose(fichier);
}

void write_modify_prenom(Personne *a , char nouveau_prenom[50]){
    // MODIFY/nom/emmanuel/koffi kouassi
    FILE* fichier = NULL;
    fichier = fopen(FILENAME , "a");

    if (fichier)
    {
        fprintf(fichier , "\nMODIFY/prenom/%s/%s" , a->nom , nouveau_prenom);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }
    fclose(fichier);
}

void write_delete(char nom[50]){
    // DELETE/nom
    FILE* fichier = NULL;
    fichier = fopen(FILENAME , "a");

    if (fichier)
    {
        fprintf(fichier , "\nDELETE/%s" , nom);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }
    fclose(fichier);
}

void write_modify_sexe(Personne *a ){
    // MODIFY/nom/emmanuel/koffi kouassi
    FILE* fichier = NULL;
    fichier = fopen(FILENAME , "a");
    char nouveau_sexe = 'M';

    if(a->sexe == 'M'){
        nouveau_sexe = 'F';
    }
    if (fichier)
    {
        fprintf(fichier , "\nMODIFY/sexe/%s/%c" , a->nom , nouveau_sexe);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }
    fclose(fichier);
}

void write_couple(Personne *a , Personne *b){
    FILE* fichier = NULL;

    fichier = fopen(FILENAME , "a");

    if (fichier)
    {
        fprintf(fichier , "\nMAKE_COUPLE/%s/%s/%s/%c" , a->nom , b->nom , b->prenom , b->sexe);
    }else{
        printf("Oups! Echec de l'ouverture du fichier");
    }

    fclose(fichier);
}


