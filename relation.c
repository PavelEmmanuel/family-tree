#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "relation.h"
#include <stdbool.h>


char *strlower(char *str){
    unsigned char *p = (unsigned char *) str;
    while(*p){
        *p = tolower((unsigned char ) *p);
        p++;
    }
    return str;
}

Personne* search(Arbre* arbre, char s[50])
{
    Personne *ptr = arbre->racine;
    if(ptr){
        strcpy(s , strlower(s));
        if(strcmp(ptr->nom,s)==0)
            return ptr;
        else if((ptr->conjoint != NULL) && (strcmp(ptr->conjoint->nom , s) == 0 ))
            return ptr->conjoint;
        else if(traverseRight(ptr,s)!=NULL)
            return traverseRight(ptr,s);
        else if(traverseDown(ptr,s)!=NULL)
            return traverseDown(ptr,s);
        else
        {
            return NULL;
        }
    }else{
        return NULL;
    }
}

int getHeightOfNode(Personne *racine , Personne* personne , int level){
    if(!racine){
        return 0;
    }
    if(racine == personne){
        return level ;
    }

    int downlevel = getHeightOfNode(racine->frere_cadet , personne , level);

    if(downlevel !=0)
        return downlevel;

    downlevel = getHeightOfNode(racine->first_child , personne , level +1);
    return downlevel;
}

void searchMultiple(Personne *personne , char s[50] , Personne **ptr){
    if(personne == NULL)
        return;

    if(strstr(personne->nom , s) || strstr(personne->prenom , s) ){
        printf("%s\n" , personne->nom);
        /*ptr = realloc(ptr , sizeof(ptr) * (i + 1));
        ptr[i] = personne;
        while(ptr[i] != NULL){
            i +=1;
        }*/
    }
    searchMultiple(personne->first_child , s , ptr);
    searchMultiple(personne->frere_cadet , s , ptr);
}

Personne* traverseDown(Personne* ptr, char s[50])
{
    // cherche tout les enfants
    ptr = ptr->first_child;

    while(ptr!=NULL)
    {
        if(strcmp(ptr->nom,s)==0 )
            return ptr;
        else if((ptr->conjoint != NULL) && (strcmp(ptr->conjoint->nom , s) == 0 ))
            return ptr->conjoint;
        else if(traverseRight(ptr,s)!=NULL)
            return traverseRight(ptr,s);
        else
            ptr = ptr->first_child;
    }
    return NULL;
}

Personne* traverseRight(Personne* ptr, char s[50])
{

    //  Chercher tous les fr�res
    ptr = ptr->frere_cadet;
    while(ptr!=NULL)
    {
        if(strcmp(ptr->nom,s)==0)
            return ptr;
        else if((ptr->conjoint != NULL) && (strcmp(ptr->conjoint->nom , s) == 0 ))
            return ptr->conjoint;
        else if (traverseDown(ptr,s)!=NULL)
            return traverseDown(ptr,s);
        else
            ptr = ptr->frere_cadet;
    }
    return NULL;
}


void findRelation(Personne *personne , int relation , Personne *racine){

    //Personne* ptrk;
    switch(relation){
        case 1: {
            //Mari�
            if(personne->conjoint != NULL)
                printf("%s est marie a %s " , personne->nom , personne->conjoint->nom);
            else
                printf("%s n'a pas de conjoint\n" , personne->nom);
            break;
        }
        case 2 : {
            //parent
            if(personne->parent != NULL){
                if(personne->parent->sexe == 'M'){
                    printf("Le pere de %s est %s\n" , personne->nom , personne->parent->nom);
                    printf("La mere de %s est %s\n" , personne->nom , personne->parent->conjoint->nom);
                }
                else{
                    printf("Le pere de %s est %s\n" , personne->nom , personne->parent->conjoint->nom);
                    printf("La mere de %s est %s\n" , personne->nom , personne->parent->nom);
                }
            }else{
                printf("Oups il semble que l'individu n'a pas de parent repertorie\n");
            }
            break;
        }
        case 3:{
            //frere
            bool is_find = false;
            Personne *frere = NULL;
            if (personne->parent){
                frere = personne->parent->first_child;
            }else{
                frere = racine;
            }

            while(frere){
                if (frere != personne){
                    printf("%s\n" , frere->nom);
                    is_find = true;
                }
                frere = frere->frere_cadet;
            }
            if(!is_find){
                printf("Aucun frere\n");
            }
            break;
        }
        case 4:{
            //cousin;
            bool is_find = false;
            if(personne->parent){
                if(personne->parent->parent){
                    Personne *oncle = personne->parent->parent->first_child;
                    while(oncle){
                        if (oncle != personne->parent){
                            Personne *enfant = oncle->first_child;
                            while(enfant){
                                printf("%s\n" , enfant->nom);
                                is_find = true;
                                enfant = enfant->frere_cadet;
                            }
                        }
                        oncle = oncle->frere_cadet;
                    }
                }
            }

            if(!is_find)
                printf("Aucun cousin\n");
            break;
        }
        case 5 : {
            //Oncle et Tante
            bool is_find = false;
            if(personne->parent){
                if(personne->parent->parent){
                    Personne *oncle = personne->parent->parent->first_child;
                    while(oncle){
                        if (oncle != personne->parent){
                            printf("%s\n" , oncle->nom);
                            is_find = true;
                        }
                        oncle = oncle->frere_cadet;
                    }
                }
            }
            if(!is_find)
                printf("Ni oncles ni tantes\n");
            break;
        }
        case 6 : {
            //Oncle
            bool is_find= false;
            if(personne->parent){
                if(personne->parent->parent){
                    Personne *oncle = personne->parent->parent->first_child;
                    while(oncle){
                        if (oncle != personne->parent && oncle->sexe == 'M'){
                            is_find = true;
                            printf("%s\n" , oncle->nom);
                        }
                        oncle = oncle->frere_cadet;
                    }
                }
            }
            if(!is_find){
                printf("Aucun oncle\n");
            }
            break;
        }
        case 7 : {
            //Tante
            bool is_find = false;
            if(personne->parent){
                if(personne->parent->parent){
                    Personne *oncle = personne->parent->parent->first_child;
                    while(oncle){
                        if (oncle != personne->parent && oncle->sexe == 'F'){
                            printf("%s\n" , oncle->nom);
                            is_find = true;
                        }
                        oncle = oncle->frere_cadet;
                    }
                }
            }
            if(!is_find)
                printf("Aucune Tante\n");
            break;
        }
        case 8 : {
            //Grand parents
            Personne *grandParent = personne->parent->parent;
            if(grandParent){
                if(grandParent->sexe == 'M'){
                    printf("Le grandpere de %s est %s\n" , personne->nom , grandParent->nom);
                    if(grandParent->conjoint){
                        printf("La grandemere de %s est %s\n" , personne->nom , grandParent->conjoint->nom);
                    }
                }
                else{
                    if (grandParent->conjoint){
                        printf("Le grandpere de %s est %s\n" , personne->nom , grandParent->nom);
                    }
                    printf("La grandemere de %s est %s\n" , personne->nom , grandParent->nom);
                }
            }else{
                printf("Aucun grand parents\n");
            }
            break;
        }
        case 9: {
            //H�ritier Oncle maternel
            bool is_find = false;
            Personne *mere = NULL;
            Personne *grandpere = NULL;
            Personne *oncle = NULL;
            mere = personne->parent;
            if(personne->parent){
                if(personne->parent->sexe == 'M' && personne->parent->conjoint != NULL){
                    mere =  personne->parent->conjoint;
                }
            }

            if(mere != NULL)
                grandpere = mere->parent;

            if(grandpere != NULL){
                oncle = grandpere->first_child;
                while(oncle != NULL){
                    if(oncle->sexe == 'M'){
                        printf("%s\n" , oncle->nom);
                        is_find = true;
                    }
                    oncle = oncle->frere_cadet;
                }
            }
            if(!is_find)
                printf("Aucun h�ritier/Oncle maternels");
            break;
        }
        case 10:{
            //Oncle paternel
            //H�ritier Oncle paternel
            bool is_find = false;
            Personne *pere = NULL;
            Personne *grandpere = NULL;
            Personne *oncle = NULL;
            pere = personne->parent;
            if(personne->parent){
                if(personne->parent->sexe == 'F'){
                    pere =  personne->parent->conjoint;
                }
            }
            if(pere != NULL)
                grandpere = pere->parent;

            if(grandpere != NULL){
                oncle = grandpere->first_child;
                while(oncle != NULL){
                    if(oncle->sexe == 'M' && oncle != pere){
                        printf("%s\n" , oncle->nom);
                        is_find = true;
                    }
                    oncle = oncle->frere_cadet;
                }
            }

            if(!is_find)
                printf("Aucun oncle paternel");
            break;
        }
        case 11 :{
            //Tante maternels || soeur de m�re
            bool is_find = false;
            Personne *mere = NULL;
            Personne *grandpere = NULL;
            Personne *oncle = NULL;
            mere = personne->parent;
            if(personne->parent->sexe == 'M' && personne->parent->conjoint != NULL){
                mere =  personne->parent->conjoint;
            }

            if(mere != NULL)
                grandpere = mere->parent;

            if(grandpere != NULL){
                oncle = grandpere->first_child;
                while(oncle != NULL){
                    if(oncle->sexe == 'F'){
                        printf("%s\n" , oncle->nom);
                        is_find = true;
                    }
                    oncle = oncle->frere_cadet;
                }
            }
            if(!is_find)
                printf("Aucune Tantes maternels \n");
            break;
        }
        case 12:{
            // Tante paternels
            bool is_find = false;
            Personne *pere = NULL;
            Personne *grandpere = NULL;
            Personne *oncle = NULL;
            pere = personne->parent;
            if(personne->parent->sexe == 'F'){
                pere =  personne->parent->conjoint;
            }

            if(pere != NULL)
                grandpere = pere->parent;

            if(grandpere != NULL){
                oncle = grandpere->first_child;
                while(oncle != NULL){
                    if(oncle->sexe == 'M' && oncle != pere){
                        printf("%s\n" , oncle->nom);
                        is_find = true;
                    }
                    oncle = oncle->frere_cadet;
                }
            }
            if(!is_find)
                printf("Aucune tante paternels\n");
            break;
        }
        case 13 : {
            //Belle soeur

            //soeur de la femme
            bool is_find = false;
            Personne* conjoint = personne->conjoint;

            Personne* parent = NULL;

            if (conjoint != NULL)
                parent = conjoint->parent;

            if (parent != NULL) {
                Personne *first_child = parent->first_child;

                if (first_child != NULL) {
                    if (first_child->sexe == 'F' && first_child != conjoint){
                        printf("%s\n" , first_child->nom);
                        is_find = true;
                    }
                    Personne * frere = first_child->frere_cadet;
                    while (frere != NULL) {
                        if (frere->sexe == 'F' && frere != conjoint){
                            printf("%s\n" , frere->nom);
                            is_find = true;
                        }
                        frere = frere->frere_cadet;
                    }
                }
            }

            //femme des freres
            parent = personne->parent;

            if (parent != NULL) {
                Personne* firstchild = parent->first_child;

                if (firstchild != NULL) {
                    if (firstchild->sexe == 'M' && firstchild != personne && firstchild->conjoint != NULL){
                        printf("%s\n" , firstchild->conjoint->nom);
                        is_find = true;
                    }

                    Personne * soeur = firstchild->frere_cadet;
                    while (soeur != NULL) {
                        if (soeur->sexe == 'M' && soeur != personne && soeur->conjoint != NULL){
                            printf("%s\n" , soeur->conjoint->nom);
                            is_find = true;
                        }
                        soeur = soeur->frere_cadet;
                    }
                }
            }

            if(!is_find)
                printf("Aucune belle soeur\n");
            break;
        }
        case 14: {
            bool is_find = false;
            //Beau fr�re

            //Fr�re du conjoint
            Personne* conjoint = personne->conjoint;

            Personne* parent = NULL;

            if (conjoint != NULL)
                parent = conjoint->parent;

            if (parent != NULL) {
                Personne *first_child = parent->first_child;

                if (first_child != NULL) {
                    if (first_child->sexe == 'M' && first_child != conjoint){
                        printf("%s\n" , first_child->nom);
                        is_find= true;
                    }

                    Personne * frere = first_child->frere_cadet;
                    while (frere != NULL) {
                        if (frere->sexe == 'M' && frere != conjoint){
                            printf("%s\n" , frere->nom);
                            is_find = true;
                        }
                        frere = frere->frere_cadet;
                    }
                }
            }

            //Mari des soeurs
            parent = personne->parent;

            if (parent != NULL) {
                Personne* firstchild = parent->first_child;

                if (firstchild != NULL) {
                    if (firstchild->sexe == 'F' && firstchild != personne && firstchild->conjoint != NULL){
                        printf("%s\n" , firstchild->conjoint->nom);
                        is_find = true;
                    }

                    Personne * soeur = firstchild->frere_cadet;
                    while (soeur != NULL) {
                        if (soeur->sexe == 'F' && soeur != personne && soeur->conjoint != NULL){
                            printf("%s\n" , soeur->conjoint->nom);
                            is_find = true;
                        }
                        soeur = soeur->frere_cadet;
                    }
                }
            }

            if(!is_find){
                printf("Aucun beau fr�re\n");
            }
            break;
        }
        case 15:{
            //Enfants
            bool is_find = false;
            Personne *firstchild = personne->first_child;

            if (firstchild != NULL) {
                printf("%s\n" , firstchild->nom);

                Personne *frere = firstchild->frere_cadet;
                while (frere != NULL) {
                    printf("%s\n" , frere->nom);
                    frere = frere->frere_cadet;
                    is_find = true;
                }
            }

            if(!is_find){
                printf("Aucuns enfants\n");
            }
            break;
        }
        case 16:{
            //Filles
            bool is_find = false;
            Personne *firstchild = personne->first_child;

            if (firstchild != NULL) {
                if (firstchild->sexe == 'F'){
                    printf("%s\n" , firstchild->nom);
                    is_find = true;
                }

                Personne *frere = firstchild->frere_cadet;
                while (frere != NULL) {
                    if (frere->sexe == 'F'){
                        printf("%s\n" , frere->nom);
                        is_find = true;
                    }
                    frere = frere->frere_cadet;
                }
            }

            if(!is_find){
                printf("Aucune filles\n");
            }
            break;
        }
        case 17:{
            //Fils
            bool is_find = false;
            Personne *firstchild = personne->first_child;

            if (firstchild != NULL) {
                if (firstchild->sexe == 'M'){
                    is_find = true;
                    printf("%s\n" , firstchild->nom);
                }

                Personne *frere = firstchild->frere_cadet;
                while (frere != NULL) {
                    if (frere->sexe == 'M'){
                        printf("%s\n" , frere->nom);
                        is_find = true;
                    }
                    frere = frere->frere_cadet;
                }
            }

            if(!is_find){
                printf("Aucun fils\n");
            }
            break;
        }

    }
}

void findRelationBetween(Arbre *arbre)
{

    char name1[50],name2[50];
    Personne* ptr1 = NULL ;
    Personne* ptr2 = NULL;

    do{
        printf("Entrer le nom de la 1ere personne : ");
        scanf("%s" , name1);
        printf("\n");
        ptr1 = search(arbre , name1);
        if(!ptr1){
            printf("%s est introuvable, Reesayez !\n" , name1);
        }
    }while(ptr1 == NULL);

    do{
        printf("Entrer le nom de la 2eme personne : ");
        scanf("%s" , name2);
        printf("\n");
        ptr2 = search(arbre , name2);
        if(!ptr2){
            printf("%s est introuvable, Reesayez !\n" , name2);
        }
    }while(ptr2 == NULL);


    Personne* ptr;
    Personne* ptrk=ptr1;
    Personne* ptrk1=ptr2;

    int ptr1_generation , ptr2_generation;
    ptr1_generation = getHeightOfNode(arbre->racine , ptr1 , 0);
    ptr2_generation = getHeightOfNode(arbre->racine , ptr2 , 0);

    switch(ptr1_generation - ptr2_generation)
    {
        char adjectif[10];
        strcpy(adjectif , "la");
        if(ptr1->sexe == 'M'){
            strcpy(adjectif , "le");
        }

    case 0:{
            char s[50];
            strcpy(s, "la femme");
            if (ptr1->sexe == 'M')
                strcpy(s, "le mari");
            ptr = ptr1->conjoint;
            if ((ptr != NULL) && (ptr == ptr2))
            {
                printf("%s est %s de %s\n" , name1 , s , name2);
                return;
            }

            strcpy(s,"la soeur");
            if(ptr1->sexe == 'M'){
                strcpy(s,"le frere");
            }
            ptr = ptr1;
            while(ptr!=NULL)
            {

                if(ptr==ptr2)
                {
                    printf("%s est %s de %s\n" , name1, s , name2);
                    return;
                }
                ptr = ptr->frere_cadet;
            }

            ptr = ptr2;
            while(ptr!=NULL)
            {
                if(ptr==ptr1)
                {
                    printf("%s est %s %s de %s\n" , name1 , adjectif , s , name2);
                    return;
                }
                ptr = ptr->frere_cadet;
            }

            printf("%s et %s sont cousins\n" , name1 , name2);
            break;
    }
    case 1:{
            char str3[50];
            strcpy(str3,"la fille");
            if(ptr1->sexe == 'M')
                strcpy(str3,"le fils");
            ptr2 = ptrk1->first_child;
            while(ptr2!=NULL)
            {
                if(ptr2==ptr1)
                {
                    printf("%s est %s de %s\n" , name1 , str3 , name2);
                    return;
                }
                ptr2=ptr2->frere_cadet;
            }
            //on fouille dans le conjoint aussi

            if(ptrk1->conjoint){
                ptr2 = ptrk1->conjoint->first_child;
                while(ptr2!=NULL)
                {
                    if(ptr2==ptr1)
                    {
                        printf("%s est %s de %s\n" , name1 , str3 , name2);
                        return;
                    }
                    ptr2=ptr2->frere_cadet;
                }
            }
            strcpy(str3,"la niece");
            if(ptr1->sexe == 'M')
                strcpy(str3,"le neveu");
            printf("%s est %s de %s\n" , name1 , str3 , name2);
            break;
    }
    case -1:{
            char str[10];
            strcpy(str,"la mere");
            if(ptr1->sexe == 'M')
                strcpy(str,"le pere");


            ptr = ptrk->first_child;
            while(ptr!=NULL)
            {
                if(ptr==ptrk1)
                {
                    printf("%s est %s de %s\n" , name1, str,  name2);
                    return;
                }
                ptr=ptr->frere_cadet;
            }
            if(ptrk->conjoint){
                ptr = ptrk->conjoint->first_child;
                while(ptr!=NULL)
                {
                    if(ptr==ptrk1)
                    {
                        printf("%s est %s de %s\n" , name1, str,  name2);
                        return;
                    }
                    ptr=ptr->frere_cadet;
                }
            }

            strcpy(str,"la tante");
            if(ptr1->sexe == 'M')
                strcpy(str,"l'oncle");
            printf("%s est %s de %s\n" , name1 , str,  name2);
            break;
    }

    case 2:{
            char str1[50];
            strcpy(str1,"la petite fille");
            if(ptr1->sexe == 'M')
                strcpy(str1,"le petit fils");

            while(ptr2!=NULL)
            {
                if(ptr2==ptr1->parent->parent)
                {
                    printf("%s est %s directe de %s\n" , name1, str1,  name2);
                    return;
                }
                if(ptr2->conjoint){
                    if(ptr2==ptr1->parent->parent->conjoint)
                    {
                        printf("%s est %s directe de %s\n" , name1, str1,  name2);
                        return;
                    }
                }
                //ptr2 = ptr2->frere_cadet;
            }
            break;
    }
    case -2:{
            char str2[50];
            strcpy(str2,"la grande mere");
            if(ptr1->sexe == 'M')
                strcpy(str2,"le grand pere");

            while(ptr1!=NULL)
            {
                if(ptr1==ptr2->parent->parent)
                {
                    printf("%s est %s de %s\n" , name1 , str2,  name2);
                    return;
                }
                if(ptr1->conjoint){
                    if(ptr1==ptr2->parent->parent->conjoint)
                    {
                        printf("%s est %s directe de %s\n" , name1, str2,  name2);
                        return;
                    }
                }
            }
            break;
    }
    default:
            printf("Relation trop longue\n");
            break;
    }
}


