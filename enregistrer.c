#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#include "relation.h"
#include "file.h"
#include "enregistrer.h"



void modifierPersonne(Personne *personne){
    int opt , c;
    printf("\n\n\t1. Modifier le nom \n\t2. Modifier les prenoms \n\t3. Modifier le sexe\n");
    printf("\tEntrez votre choix : ");
    scanf("%d" , &opt);
    while ((c = getchar()) != '\n' && c != EOF) { }
    switch(opt){
        case 1:{
            char newName[50];
            printf("Entrez le nouveau nom : ");
            scanf("%s", newName);
            while ((c = getchar()) != '\n' && c != EOF) { }
            write_modify_nom(personne , newName);
            strcpy(personne->nom , newName);
            printf("Operation effectue avec success\n");
            break;
        }
        case 2 :{
            char newName[50];
            printf("Entrez le nouveau prenom : ");
            scanf("%[^\n]s" , newName);
            while ((c = getchar()) != '\n' && c != EOF) { }
            write_modify_prenom(personne , newName);
            strcpy(personne->prenom , newName);
            printf("Operation effectue avec success\n");
            break;
        }
        case 3:{
            if(personne->sexe == 'M'){
                personne->sexe = 'F';
            }else{
                personne->sexe = 'M';
            }
            write_modify_sexe(personne);
            printf("Operation effectue avec success\n");
            break;
        }
        default:{
            printf("Oups! la valeur saisie est incorrecte\n");
        }

    }
}

void saisirPersonne(Personne* personne){
    int c ;
    printf("Entrer le nom de la personne : ");
    scanf("%s" , personne->nom);
    while ((c = getchar()) != '\n' && c != EOF) { }
    strcpy(personne->nom , strlower(personne->nom));
    printf("Enter les prenoms de la personne : ");
    scanf("%[^\n]s", personne->prenom);
    while ((c = getchar()) != '\n' && c != EOF) { }
    strcpy(personne->prenom , strlower(personne->prenom));
    do{
        printf("Entrer le sexe de la personne (M/F) : ");
        scanf("%c" , &personne->sexe);
        while ((c = getchar()) != '\n' && c != EOF) { }
        personne->sexe = toupper(personne->sexe);
        if(personne->sexe != 'F' && personne->sexe != 'M')
            printf("La valeur entree est incorrecte ! Reessayez\n");
    }while(personne->sexe != 'F' && personne->sexe != 'M');
}

void deletePersonne(Arbre* arbre , Personne* personne){
    Personne *parent = personne->parent;
    char nom[50];
    strcpy(nom , personne->nom);
    if(parent){
        Personne *brother = parent->first_child;
        if(brother == personne){
            parent->first_child = personne->frere_cadet;
        }else{
            while (brother->frere_cadet != personne){
                brother = brother->frere_cadet;
            }
            brother->frere_cadet = personne->frere_cadet;
            //printf("%s = %s"  , brother->nom , brother->frere_cadet->nom);
        }
        destroyNode(personne->first_child);
        /*if(personne->conjoint){
            free(personne->conjoint);
        }*/
        free(personne);
    }else{
        destroy(arbre , personne);
    }
    write_delete(nom);
}

void destroyNode(Personne *ptr){
    Personne *temp = ptr;
    if(ptr == NULL){
        return;
    }

    while (ptr != NULL){
        destroyNode(ptr->first_child);
        temp = ptr;
        ptr = ptr->frere_cadet;
        free(temp);
    }
}
void destroy(Arbre* arbre, Personne* ptr)
{
    Personne* temp = ptr;

    if(ptr==NULL)
        return;

    while(ptr!=NULL)
    {
        destroy(arbre, ptr->first_child);
        temp = ptr;
        ptr = ptr->frere_cadet;
        free(temp);
    }

    arbre->racine = NULL;
}
void ajouterPersonne(Arbre *arbre){
    Personne* personne = (Personne*) malloc(sizeof(Personne));
    Personne* relation = NULL;

    if (personne != NULL){
        initialiserPersonne(personne);
        saisirPersonne(personne);
    }else{
        printf("Oups ! �chec de l'allocation de la m�moire ! ");
    }

    if(arbre->racine == NULL)
    {
        arbre->racine = personne;
        write_root(personne);
    }else{
        char nom[50];
        char child[10];
        char brother[10];
        char adjectif[3];
        int opt , c;
        strcpy(child , "fille");
        strcpy(brother , "soeur");
        strcpy(adjectif , "la");
        if(personne->sexe == 'M'){
            strcpy(child , "fils");
            strcpy(brother , "frere");
            strcpy(adjectif , "le");
        }
        do{
            printf("Entrer le nom d'un menbre de la famille : ");
            scanf("%s" , nom);
            while ((c = getchar()) != '\n' && c != EOF) { }
            relation = search(arbre , strlower(nom));
            if(relation){
                printf("Personne selectionne : %s %s\n" , relation->nom , relation->prenom);
                printf("Entrez le nom de la relation \n 1 - %s \n 2 - %s\n 3 - Conjoint(e)\n" , child , brother);
                printf("%s est %s ______ de %s : " , personne->nom , adjectif , nom);
                scanf("%d", &opt);
                switch(opt){
                    case 1 :{
                        ajouterEnfant(relation, personne);
                        write_child(relation , personne);
                        printf("La personne a ete ajoute avec success\n\n");
                        break;
                    }
                    case 2:{
                        ajouterFrere(relation, personne);
                        write_child(relation->parent , personne);
                        printf("La personne a ete ajoute avec success\n\n");
                        break;
                    }
                    case 3 :{
                        ajouterConjoint(relation , personne);
                        write_couple(relation , personne);
                        break;
                    }
                    default :
                        printf("La valeur saisi ne figure pas dans la liste ! Reessayez");
                }
            }else{
                printf("Le nom saisi ne fait pas partie de la famille ! Reessayez\n");
            }
        }while(relation == NULL);
    }
}

void initialiserPersonne(Personne *personne){
    char name[1] = "";
    personne->first_child = NULL;
    personne->frere_cadet = NULL;
    strcpy(personne->nom, name);
    personne->parent = NULL;
    strcpy(personne->prenom , name);
    personne->sexe = 'M';
    personne->conjoint = NULL;
}

void ajouterConjoint(Personne *a , Personne *b){
    //b est ajoute comme conjoint de a
    if(a->conjoint != NULL){
        printf("%s est d�j� mari�\n" , a->nom);
        return;
    }else{
        if(b->sexe != a->sexe){
            a->conjoint = b;
            b->conjoint = a;
            printf("%s est maintenant marie a %s\n" , b->nom , a->nom);
        }else{
            printf("Oups ! les conjoints ne peuvent pas etre de meme sexe\n");
            return;
        }
    }
}

void ajouterFrere(Personne* a, Personne* b)
{
    // b est ajout� comme fr�re de a

    while(a->frere_cadet !=NULL)
        a=a->frere_cadet;
    a->frere_cadet = b;
    b->parent = a->parent;
}

void ajouterEnfant(Personne* a,Personne* b)
{
    //b est ajout� comme enfant de a ou b est ajout� comme fr�re cadet du dernier enfant de a

    if(a->first_child == NULL){
        a->first_child = b;
        b->parent =a ;
    }
    else{
        ajouterFrere(a->first_child,b);
    }
}
